FROM alpine:3.9
LABEL author="Tyler Christiansen <code@tylerc.me>"

RUN adduser -S batfish

EXPOSE 8888

RUN apk add --no-cache git py3-pip py3-zmq \
    && pip3 install jupyter
RUN apk add --no-cache g++ python3-dev \
  && pip3 install git+https://github.com/batfish/pybatfish.git \
  && apk del g++ python3-dev

USER batfish

ENTRYPOINT [ "jupyter", "notebook", "--ip=0.0.0.0", "--port=8888" ]
